import React, { Component } from "react";
import { Charts, CountryPicker, Cards } from "./components";
import styles from "./App.module.css";
// importing fetched data
import { fetchData } from "./api";

export default class App extends Component {
  state = {
    data: {},
    country: "",
  };
  async componentDidMount() {
    // awaiting requested data
    const fetchedData = await fetchData();
    console.log(fetchedData);
    this.setState({
      data: fetchedData,
    });
    // console.log(data);
  }
  handleCountryChange = async (country) => {
    console.log(country);
  };
  render() {
    const { data } = this.state;
    return (
      <div className={styles.container}>
        {/* passing props to card component */}
        <Cards data={data} />
        <CountryPicker />
        <Charts />
      </div>
    );
  }
}
