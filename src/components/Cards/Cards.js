import React from "react";
import styles from "./Cards.module.css";
// import styles from "../App.module.css";
import { Card, CardContent, Typography, Grid } from "@material-ui/core";
import Countup from "react-countup";
import cx from "classnames";
// passing props
export default function Cards({
  data: { confirmed, recovered, deaths, lastUpdate },
}) {
  // checking if data is there
  if (!confirmed) {
    return "Loading...";
  }
  // console.log({confirmed});
  return (
    <div className={styles.container}>
      <Grid container spacing={3} justify="center">
        <Grid
          item
          component={Card}
          xs={12}
          md={3}
          className={cx(styles.card, styles.infected)}
        >
          <CardContent>
            <Typography color="textSecondary" gutterBottom>
              Infected
            </Typography>
            <Countup
              start={0}
              end={confirmed.value}
              duration={2.5}
              separator=","
            />
            {/* <Typography variant="h5">{confirmed.value}</Typography> */}
            <Typography color="textSecondary">
              {new Date(lastUpdate).toDateString()}
            </Typography>
            <Typography variant="body2">
              Number of active cases of COVID-19
            </Typography>
          </CardContent>
        </Grid>
        <Grid
          item
          component={Card}
          xs={12}
          md={3}
          className={cx(styles.card, styles.recovered)}
        >
          <CardContent>
            <Typography color="textSecondary" gutterBottom>
              Recovered
            </Typography>
            <Countup
              start={0}
              end={recovered.value}
              duration={2.5}
              separator=","
            />
            {/* <Typography variant="h5">Real Data</Typography> */}
            <Typography color="textSecondary">
              {new Date(lastUpdate).toDateString()}
            </Typography>
            <Typography variant="body2">
              Number of recovered cases from COVID-19
            </Typography>
          </CardContent>
        </Grid>
        <Grid
          item
          component={Card}
          xs={12}
          md={3}
          className={cx(styles.card, styles.deaths)}
        >
          <CardContent>
            <Typography color="textSecondary" gutterBottom>
              Deaths
            </Typography>
            <Countup
              start={0}
              end={deaths.value}
              duration={2.5}
              separator=","
            />
            {/* <Typography variant="h5">Real Data</Typography> */}
            <Typography color="textSecondary">
              {new Date(lastUpdate).toDateString()}
            </Typography>
            <Typography variant="body2">
              Number of deaths caused by COVID-19
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </div>
  );
}
