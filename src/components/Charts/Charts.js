import React, { useEffect, useState } from "react";
import { fetchDailyData } from "../../api";
import { Line } from "react-chartjs-2";
import styles from "./Charts.module.css";

export default function Charts() {
  const [dailyData, setDailyData] = useState([]);
  useEffect(() => {
    const fetchApi = async () => {
      // setting state to fetched data
      setDailyData(await fetchDailyData());
    };
    // console.log(dailyData);c
    fetchApi();
  }, [setDailyData]);
  //if first day daily data avalable, display line chart
  const lineChart = dailyData.length ? (
    <Line
      data={{
        // looping to get date
        labels: dailyData.map(({ date }) => date),
        datasets: [
          {
            data: dailyData.map(({ confirmed }) => confirmed),
            label: "Infected",
            borderColor: "#3333ff",
            fill: true,
          },
          {
            data: dailyData.map(({ deaths }) => deaths),
            label: "Deaths",
            borderColor: "red",
            backgroundColor: "rgba(255,0,0,0.5)",
            fill: true,
          },
        ],
      }}
    />
  ) : (
    0
  );
  return <div className={styles.container}>{lineChart}</div>;
}
