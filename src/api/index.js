import axios from "axios";
// general data api
const generalUrl = "https://covid19.mathdro.id/api";

const dailyUrl = "https://covid19.mathdro.id/api/daily";
// const countriesUrl = "https://covid19.mathdro.id/api/countries";
export const fetchData = async () => {
  try {
    const {
      // destructuring required data
      data: { confirmed, recovered, deaths, lastUpdate },
    } = await axios.get(generalUrl);
    return { confirmed, recovered, deaths, lastUpdate };
    // const modifiedData = {
    //   confirmed,
    //   recovered,
    //   deaths,
    //   lastUpdate,
    // };
    // return modifiedData;

    // return data;
    // console.log(data);
  } catch (error) {
    console.log(error);
  }
};
// daily data
export const fetchDailyData = async () => {
  try {
    const { data } = await axios.get(dailyUrl);
    console.log(data);
    const dailyModifiedData = data.map((dailyData) => ({
      confirmed: dailyData.confirmed.total,
      deaths: dailyData.deaths.total,
      recovered: dailyData.recovered.total,
      date: dailyData.reportDate,
    }));
    return dailyModifiedData;
  } catch (error) {
    console.log(error);
  }
};

// bar graph country data
export const fetchCountries = async () => {
  try {
    const {
      data: { countries },
    } = await axios.get(`${generalUrl}/countries`);
    return countries.map((country) => country.name);
    // console.log(data);
  } catch (error) {
    console.log(error);
  }
};
